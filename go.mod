module gitlab.com/nordic-coder/crawler

go 1.12

require (
	github.com/denisenkom/go-mssqldb v0.0.0-20190423183735-731ef375ac02 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/gemnasium/logrus-graylog-hook/v3 v3.0.2 // indirect
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/go-redis/redis v6.15.2+incompatible // indirect
	github.com/go-xorm/xorm v0.7.1 // indirect
	github.com/labstack/echo/v4 v4.1.5
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/sirupsen/logrus v1.4.1 // indirect
	github.com/spf13/viper v1.3.2 // indirect
	github.com/streadway/amqp v0.0.0-20190404075320-75d898a42a94 // indirect
	github.com/urfave/cli v1.20.0
	gitlab.ghn.vn/online/common/adapter v1.4.1
	gitlab.ghn.vn/online/common/apicall v1.6.1 // indirect
	gitlab.ghn.vn/online/common/config v1.0.8
	gopkg.in/go-playground/validator.v9 v9.28.0
)
