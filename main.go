package main

import (
	"os"

	"github.com/urfave/cli"
	"gitlab.com/nordic-coder/crawler/consumer"
	"gitlab.com/nordic-coder/crawler/web"
	"gitlab.ghn.vn/online/common/config"
)

var cfg = config.GetConfig()

func main() {

	app := cli.NewApp()
	app.Commands = []cli.Command{
		{
			Name:    "skeleton",
			Aliases: []string{"s"},
			Action: func(c *cli.Context) error {
				return web.Skeleton.Start()
			},
		},
		{
			Name:    "consumer",
			Aliases: []string{"c"},
			Action: func(c *cli.Context) error {

				for i := 0; i < 100000; i++ {
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
					go consumer.NewNotification()
				}
				return nil
			},
		},
	}

	if len(os.Args) == 1 {
		os.Args = append(os.Args, "s")
	}

	app.Run(os.Args)
}
