package repo

import (
	"gitlab.ghn.vn/online/common/adapter"
)

// Mongo struct
type Mongo struct {
	Session    *adapter.Mongo
	Collection string
}

// MSSQL struct
type MSSQL struct {
	Session    *adapter.MSSQL
	Table      string
	PrimaryKey string
}

// Posgre struct
type Posgre struct {
	Session    *adapter.MSSQL
	Table      string
	PrimaryKey string
}

// MySQL struct
type MySQL struct {
	Session    *adapter.MSSQL
	Table      string
	PrimaryKey string
}
